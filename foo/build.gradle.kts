plugins {
    application
}

application {
    mainClassName = "org.mpierce.gradleintro.Foo"
}

val deps: Map<String, String> by extra

dependencies {
    testCompile("org.junit.jupiter:junit-jupiter-api:${deps["junit"]}")
    testRuntime("org.junit.jupiter:junit-jupiter-engine:${deps["junit"]}")
}

tasks.test {
    useJUnitPlatform()
}

val doStuff by tasks.registering {
    doLast {
        println("doing stuff!")
    }
}

tasks.compileJava {
    dependsOn(doStuff)
}
