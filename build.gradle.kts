subprojects {
    apply<JavaPlugin>()

    val deps by extra {
        mapOf("junit" to "5.4.2")
    }

    repositories {
        jcenter()
    }
}
